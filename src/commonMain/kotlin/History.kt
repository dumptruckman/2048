import com.soywiz.kds.iterators.fastForEach
import com.soywiz.korge.view.Container

class History(from: String?, private val onUpdate: (History) -> Unit) {

    class Element(val numberIds: IntArray, val score: Int)

    private val history = mutableListOf<Element>()

    val currentElement: Element get() = history.last()

    init {
        from?.split(';')?.fastForEach {
            val element = elementFromString(it)
            history.add(element)
        }
    }

    private fun elementFromString(string: String): Element {
        val numbers = string.split(',').map { it.toInt() }
        if (numbers.size != 17) throw IllegalArgumentException("Incorrect history")
        return Element(IntArray(16) { numbers[it] }, numbers[16])
    }

    fun add(numberIds: IntArray, score: Int) {
        history.add(Element(numberIds, score))
        onUpdate(this)
    }

    fun undo(): Element {
        if (history.size > 1) {
            history.removeAt(history.size - 1)
            onUpdate(this)
        }
        return history.last()
    }

    fun clear() {
        history.clear()
        onUpdate(this)
    }

    fun isEmpty() = history.isEmpty()

    override fun toString(): String {
        return history.joinToString(";") {
            it.numberIds.joinToString(",") + "," + it.score
        }
    }
}

fun Container.restoreField(history: History.Element) {
    map.forEach { if (it != -1) deleteBlock(it) }
    map = PositionMap()
    score.update(history.score)
    freeId = 0
    val numbers = history.numberIds.map {
        if (it >= 0 && it < Number.values().size) {
            Number.values()[it]
        } else {
            null
        }
    }
    numbers.forEachIndexed { i, number ->
        if (number != null) {
            val newId = createNewBlock(number, Position(i % 4, i / 4))
            map[i % 4, i / 4] = newId
        }
    }
}